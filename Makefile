
include makefile.config

ibprof clean veryclean:
	cd profiler; make -f makefile.ibprof $@

install :
	if [ -d ${IBPROF_INSTALL_PATH} ]; then \
		${MAKE} hardpush;\
	fi
	if [ ! -d ${IBPROF_INSTALL_PATH} ]; then \
		${MAKE} showpath;\
	fi

hardpush :
	mkdir -p ${IBPROF_INSTALL_PATH}/lib
	mkdir -p ${IBPROF_INSTALL_PATH}/bin
	mkdir -p ${IBPROF_INSTALL_PATH}/include
	${CP} profiler/libibprof.*.so* ${IBPROF_INSTALL_PATH}/lib/
	${CP} profiler/libibprof.so ${IBPROF_INSTALL_PATH}/lib/
	${CP} profiler/ibprof.h ${IBPROF_INSTALL_PATH}/include/
	${CP} converter/* ${IBPROF_INSTALL_PATH}/bin/
	for SCRIPT in ${IBPROF_SCRIPTS}; do\
		echo "chmod +x ${IBPROF_INSTALL_PATH}/bin/$${SCRIPT}";\
		chmod +x ${IBPROF_INSTALL_PATH}/bin/$${SCRIPT};\
	done

showpath:
	echo ""
	echo "[****** ERROR *******] Could not find installation directory. Please ensure IBPROF_INSTALL_PATH is set correctly."
