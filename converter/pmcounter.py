# -*- coding: utf-8 -*-
"""
Copyright 2014 Kevin A. Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

""" pmcounter.py

    Input:  filename of ibdiagnet output file (eg. "ibdiagnet2.pm")
    Output: dictionary of ports and counter values
"""

import sys, os, re

def parsefile(filename=''):
    pc = {}

    pmfile = open(filename)
    if pmfile.closed:
        print

    headline = re.compile('Port=(\w+)\s+Lid=0x(\w+)\s+GUID=0x(\w+)\s+Device=(\w+)\s+Port Name=(.+)/(\w+)')
    xmitline = re.compile('port_xmit_data_extended=(\w+)')
    recvline = re.compile('port_rcv_data_extended=(\w+)')

    def hextoint(val):
        if val.startswith('0x'):
            val = val[len('0x'):]
        else:
            val = '0'
        return int(val, 16)

    # We start a section everytime a headline is matched
    section = False
    xmitval = recvval = pguid = None
    for line in pmfile:
        if section:
            if xmitline.match(line):
                xmitval = hextoint(xmitline.match(line).group(1))
            elif recvline.match(line):
                recvval = hextoint(recvline.match(line).group(1))
            else:
                continue

            if xmitval != None and recvval != None:
                # IB port counters increments by 1 for every 4 bytes
                xmitval *= 4
                recvval *= 4
                if pc.has_key(pguid):
                    pc[pguid][pn] = {'xmit': xmitval, 'recv': recvval}
                else:
                    pc[pguid] = {pn: {'xmit': xmitval, 'recv': recvval}}

                section = False
                recvval = pguid = None

        elif headline.match(line):
            m = headline.match(line)
            pn, lid, pguid, dev, name1, name2 = int(m.group(1)), \
                                                int(m.group(2), 16),  \
                                                m.group(3), \
                                                m.group(4), \
                                                m.group(5), \
                                                m.group(6)
            section = True

    pmfile.close()
    return pc

""" Calculate the change in port counter values

    Input:  pc1     - dictionary of initial port counter values
            pc2     - dictionary of final port counter values

    Output: pcdiff  - dictionary of change in port counter values
"""
def getdiff(pc1, pc2):
    pcdiff = {}

    pc1keys = [(pn, pguid) for pguid in pc1.keys() for pn in pc1[pguid].keys()]
    pc2keys = [(pn, pguid) for pguid in pc2.keys() for pn in pc2[pguid].keys()]

    ports = list( set(pc1keys) | set(pc2keys) )# | set(netkeys) )
    for (pn, pguid) in ports:
        if pc1keys.count((pn, pguid)) == 0 and pc2keys.count((pn, pguid)) == 0:
            if pcdiff.has_key(pguid):
                pcdiff[pguid][pn] = {'xmit': 0, 'recv': 0}
            else:
                pcdiff[pguid] = {pn: {'xmit': 0, 'recv': 0}}

            sys.stderr.write("[WARNING] PGUID: " + pguid +
                                " not found in both port counter files.\n")
            continue

        xmit = pc2[pguid][pn]['xmit'] - pc1[pguid][pn]['xmit']
        recv = pc2[pguid][pn]['recv'] - pc1[pguid][pn]['recv']

        xmit = xmit if xmit > 0 else 0
        recv = recv if recv > 0 else 0

        if pcdiff.has_key(pguid):
            pcdiff[pguid][pn] = {'xmit': xmit, 'recv': recv}
        else:
            pcdiff[pguid] = {pn: {'xmit': xmit, 'recv': recv}}

    return pcdiff
