#!/home/users/kevin/opt/python-2.7.9/bin/python
# -*- coding: utf-8 -*-
"""
Copyright 2014 Kevin A. Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

""" version: 1.1
    NOTES:
     profiles were written with the following commands:
            OTF_WStream_writeDefProcess(ibprofiler.otf_stream, ibprofiler.mpirank, ibprofiler.hostname, 0);
            OTF_WStream_writeDefCounterKV(ibprofiler.otf_stream, ibprofiler.send_xfers[POS(i,0)],
                str, OTF_COUNTER_TYPE_ABS + OTF_COUNTER_SCOPE_POINT, ibprofiler.mpirank, "bytes sent", NULL);

            OTF_KeyValueList_appendUint64( KeyValueList, 0, ibprofiler.send_xfers[POS(i,j)]);  --- sent data
            OTF_KeyValueList_appendUint64( KeyValueList, 1, ibprofiler.recv_xfers[POS(i,j)]);  --- recv data

            OTF_WStream_writeCounterKV( ibprofiler.otf_stream, otf_code_region,
                        ibprofiler.mpirank, ibprofiler.send_xfers[POS(i,0)], j, KeyValueList );

    """

from pprint import pprint as pp
from otf import *
import sys

def handleDefProcGroup( user_data, stream, group, name, num_procs, proc_list, kv_list ):

    return OTF_RETURN_OK

def handleDefProcess( user_data, stream, process, name, parent, kvlist ):

    user_data[process] = {'hostname': name}

    return OTF_RETURN_OK

def handleDefCounter( user_data, stream, counter, name, properties, group, unit, kvlist ):

    port_guid = name.strip()[2:]
    source_lid = counter
    user_data[group][(port_guid, source_lid)] = {}

    return OTF_RETURN_OK

def handleCounter( user_data, time, process, counter, value, kvlist ):

    dest_lid = int(value)
    source_lid = counter
    region = int(time)

    ret_val_s, bytes_sent = OTF_KeyValueList_getUint64(kvlist, 0)
    ret_val_r, bytes_recv = OTF_KeyValueList_getUint64(kvlist, 1)

    port_guid = [val[0] for val in user_data[process].keys() if val[1] == source_lid][0]

    if not user_data[process].has_key((port_guid,source_lid)):
        user_data[process][(port_guid,source_lid)] = {}
    if not user_data[process][(port_guid,source_lid)].has_key(dest_lid):
        user_data[process][(port_guid,source_lid)][dest_lid] = {}

    if ret_val_s == 0:
        if user_data[process][(port_guid,source_lid)][dest_lid].has_key('sent'):
            user_data[process][(port_guid,source_lid)][dest_lid]['sent'][region] = bytes_sent
        else:
            user_data[process][(port_guid,source_lid)][dest_lid]['sent'] = {region: bytes_sent}
    if ret_val_r == 0:
        if user_data[process][(port_guid,source_lid)][dest_lid].has_key('recv'):
            user_data[process][(port_guid,source_lid)][dest_lid]['recv'][region] = bytes_recv
        else:
            user_data[process][(port_guid,source_lid)][dest_lid]['recv'] = {region: bytes_recv}

    return OTF_RETURN_OK

class profReader:

    records = {}

    def parseProfile(self, profile):
        records  = self.records

        global handleDefProcess
        global handleDefCounter
        global handleCounter

        manager= OTF_FileManager_open( 100 )
        reader= OTF_Reader_open( profile, manager )
        handlers= OTF_HandlerArray_open()

        OTF_HandlerArray_setHandler( handlers, handleDefProcess, OTF_DEFPROCESS_RECORD )
        OTF_HandlerArray_setHandler( handlers, handleDefCounter, OTF_DEFCOUNTER_RECORD )
        OTF_HandlerArray_setHandler( handlers, handleCounter, OTF_COUNTER_RECORD )

        OTF_HandlerArray_setFirstHandlerArg( handlers, records, OTF_DEFPROCESS_RECORD )
        OTF_HandlerArray_setFirstHandlerArg( handlers, records, OTF_DEFCOUNTER_RECORD )
        OTF_HandlerArray_setFirstHandlerArg( handlers, records, OTF_COUNTER_RECORD )

        OTF_Reader_readDefinitions( reader, handlers )
        read= OTF_Reader_readEventsUnsorted( reader, handlers )

    def getData(self):
        return self.records

    def __init__(self, profile):
        self.parseProfile(profile)

if __name__ == "__main__":
    app = profReader(sys.argv[1])
    print 'RECORDS:'
    pp(app.getData())
