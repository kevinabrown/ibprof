#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pprint import pprint as pp

import operator
import re

""" Reads a hostfile list and assumes processes are assigned in sequetial order
    with a 1:1 to host:rank mapping
"""
def getNewMap():
    new_map = {}

    # Get filename from user
    fname = raw_input('Please enter name and paths to new map file: ')
    if len(fname) == 0:
        return None

    # Attempts to open file
    map_file = open(fname)

    # Assign MPI rank (starting from 0) to host based on the order in the list
    i = 0
    for line in map_file:
        line = line.replace('\n', '')
        new_map[i] = line;
        i += 1

    return new_map


""" Show communication summary over specified link port
"""
def showTraffic(comms):

    stats = {'regions':{}}
    outtxt = 'source,dest,region,data\n'

    for (procSrc, procDst, traffic) in comms:

        procSrc = procSrc[0]
        procDst = procDst[0]

        if not stats.has_key(procSrc):
            stats[procSrc] = {'dst': 0, 'snd': 0, 'src': 0, 'rcv': 0}
        if not stats.has_key(procDst):
            stats[procDst] = {'dst': 0, 'snd': 0, 'src': 0, 'rcv': 0}

        stats[procSrc]['dst'] += 1
        stats[procDst]['src'] += 1

        for region, data in traffic.items():
            stats[procSrc]['snd'] += data
            stats[procDst]['rcv'] += data

            if not stats['regions'].has_key(region):
                stats['regions'][region] = 0

            stats['regions'][region] += data
            outtxt += str(procSrc) + ',' + str(procDst) + ',' + str(region) + ',' + str(data) + '\n'

    pp(stats)

    outfile = open('outfile', 'w')
    outfile.write(outtxt)





