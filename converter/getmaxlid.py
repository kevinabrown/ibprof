#!/usr/bin/env python
# -*- coding: utf-8 -*-

# The function get_maxlid checks all ibdiagnet.pkey file in a give path
# and return the value of the maximum LID of all ports

import os
import re
import sys
import time

def get_maxlid(net_dir):
    if os.path.isdir(net_dir) == False:
        sys.exit('Network directory not found.')

    pkey_files = []
    for root, dirs, files in os.walk(net_dir):
        for fname in files:
            if fname.endswith('.pkey'):
                pkey_files.append(str(root) + '/' + fname)

    if len(pkey_files) == 0:
        sys.exit('No ibdiagnet.pkey files found in the given path.')

    regex = re.compile('\s+(\w+)\s(\S+)/(\w+)/(\w+)\slid=(\w+)\s\.*')

    maxlid = 0
    for filename in pkey_files:
        pkey_file = open(filename, 'r')

        for line in pkey_file:
            if regex.match(line):
                m = regex.match(line)

                type, hname, un, pn, lid = m.group(1), m.group(2), m.group(3),\
                                           m.group(4), int(m.group(5), 16)

                if lid > maxlid:
                    maxlid = lid
    return maxlid

""" If an argument is passed to the script, use it as the path to ibdiagnet log
    files for the network. If no path is give, use the default location on TSU-
    BAME2.5. TSUBAME2.5 log files for the previous hour is used.

    default location format:
        /gsic/system/gsic/ib_counter_check/log/yyyymmdd/hhmm

        where mm is '05'
"""
if __name__ == "__main__":
        if len(sys.argv) > 1:
            netdir = sys.argv[1]
        else:
            now = time.time()

            # find the time of hour ago
            ago = time.localtime(now - 3600)

            # convert int to str with leading zeros
            yr = str(ago.tm_year).zfill(4)
            mn = str(ago.tm_mon ).zfill(2)
            dy = str(ago.tm_mday).zfill(2)
            hr = str(ago.tm_hour).zfill(2)
            min = '05'

            ibdpath = '/gsic/system/gsic/ib_counter_check/log'
            netdir  = ibdpath + '/' + yr + mn + dy +'/' + hr + min

        maxlid = get_maxlid(netdir)

        print maxlid
