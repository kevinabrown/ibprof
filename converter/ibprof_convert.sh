#!/bin/sh

# 	ibprof_convert.sh - helps convert from ibprof profiles to Boxfish YAML files
#
#	This script is only necessary because we're usnig a later version of python and otf wrappers for python
#	Usage:
#		ibprof_convert.sh <path_to_ibdiagnet_output-rail1> [ <path_to_ibdiagnet_output-rail1> ] \
#					<profile_name>	<yaml_output>

BASE_PATH="/work1/t2g-ebdcrest/apps"
OTF_DIR="otf-1.12.4salmon"
IBPROF_DIR="ibprof-0.1"
PYTHON_DIR="python-2.7.9"

OLD_PYTHONPATH=${PYTHONPATH}
export PYTHONPATH=${BASE_PATH}/${OTF_DIR}/lib64/python2.6/site-packages:${PYTHONPATH}

${BASE_PATH}/${PYTHON_DIR}/bin/python ${BASE_PATH}/${IBPROF_DIR}/bin/ibprof2Boxfish.py $1 $2 $3 $4

export PYTHONPATH=${OLD_PYTHONPATH}
