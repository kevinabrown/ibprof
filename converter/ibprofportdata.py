#!/home/users/kevin/opt/python-2.7.9/bin/python
"""
        IMPORTANT: This only works for single rail networks

        Prints profile per port. That is, data sent/recv on each port
        - Uses ibprofparser.py
"""

import sys
from pprint import pprint as pp
import ibprofparser

def print_usage():
    print 'This scripts only works for single-rail networks.'
    print 'Usage:'
    print '     # ibprofportdata.py <profile_name>'
    sys.exit()

if __name__ == "__main__":

    if len(sys.argv) < 2:
        print ' *** You need to provide the name of a profile *** '
        print ''
        print_usage()
    if len(sys.argv) > 3:
        print ' *** Too many arguments recieved *** '
        print ''

    metric = {}
    pguid_dict={}

    run = ibprofparser.profReader(sys.argv[1])
    prof_data = run.getData()

    for mpirank in prof_data.keys():
        hname = prof_data[mpirank]['hostname'].strip()
        metric[hname] = {}

        for src_info in prof_data[mpirank].keys():
            if src_info != 'hostname':
                metric[hname][src_info] = {'sent': 0, 'recv': 0}
                pguid_dict[src_info[1]] = (src_info[0], hname)

    for mpirank in prof_data.keys():
        hname = prof_data[mpirank]['hostname'].strip()

        for src_info in prof_data[mpirank].keys():
            if src_info != 'hostname':

                for dst_lid, data in prof_data[mpirank][src_info].items():
                    for op, traffic in data.items():
                        dst_info = (pguid_dict[dst_lid][0], dst_lid)
                        dname = pguid_dict[dst_lid][1]

                        if op == 'sent':
                            for data in traffic.values():
                                metric[hname][src_info]['sent'] += data
                                metric[dname][dst_info]['recv'] += data

                        elif op == 'recv':
                            for data in traffic.values():
                                metric[hname][src_info]['recv'] += data
                                metric[dname][dst_info]['sent'] += data

    pp(metric)
