#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <inttypes.h>
#include <unistd.h>
#include <sys/utsname.h>
#include <sys/unistd.h>
#include <sys/stat.h>	//mkdir
#include <stdbool.h>

#include "ibprof.h"
#include "ibprof_internal.h"
#include "ibprof_pmpi.h"	/* must be included after ibprof.h */

/* Required for checking the local IB LID */
#include <infiniband/verbs.h>
#if defined(__linux__)
#  include <endian.h>
#elif defined(__FreeBSD__) || defined(__NetBSD__)
#  include <sys/endian.h>
#elif defined(__OpenBSD__)
#  include <sys/types.h>
#  define be16toh(x) betoh16(x)
#  define be32toh(x) betoh32(x)
#  define be64toh(x) betoh64(x)
#endif

/* Case-insensitive string comparision  function */
int strcmp_nocase(const char *p1, const char *p2){
	unsigned char c1, c2;

	do{
		c1 = (unsigned char) toupper((int)*p1++);
		c2 = (unsigned char) toupper((int)*p2++);
		if (c1 == '\0' || c2 == '\0'){
			return c1 - c2;
		}
	}while (c1 == c2);

	return c1 - c2;
}

/* Wrapper for MPI_Init_thread */
int MPI_Init_thread(int *argc, char ***argv, int required, int *provided){
	int ret;
	extern char *__progname;
	char filename[20];

	strcpy(filename, "ibprofile/");
	mkdir(filename, ACCESSPERMS); /*TODO: Verify that no error is returned*/

	ret = PMPI_Init_thread(argc, argv, required, provided);

#if IBPROF_GET_TIME
	MPI_Barrier(MPI_COMM_WORLD);
	gettimeofday(&st, NULL);
#endif

	MPI_Comm_rank(MPI_COMM_WORLD, &ibprofiler.mpirank);
	MPI_Comm_size(MPI_COMM_WORLD, &ibprofiler.mpisize);

	if (ibprofiler.inited == 0) {
		if (strlen(__progname) > 0)
			strncat(filename, __progname, 10);
		else
			strncat(filename, "default", 7);

		IBProf_Init(filename);
	}

	return ret;
}

/* Wrapper for MPI_Init */
int MPI_Init(int *argc, char ***argv){
	int ret;
	extern char *__progname;
	char filename[20];

	strcpy(filename, "ibprofile/");
	mkdir(filename, ACCESSPERMS); /*TODO: Verify that no error is returned*/

	ret = PMPI_Init(argc, argv);

#if IBPROF_GET_TIME
	MPI_Barrier(MPI_COMM_WORLD);
	gettimeofday(&st, NULL);
#endif

	MPI_Comm_rank(MPI_COMM_WORLD, &ibprofiler.mpirank);
	MPI_Comm_size(MPI_COMM_WORLD, &ibprofiler.mpisize);

	if (ibprofiler.inited == 0) {
		if (strlen(__progname) > 0)
			strncat(filename, __progname, 10);
		else
			strncat(filename, "default", 7);

		IBProf_Init(filename);
	}

	return ret;
}

int MPI_Finalize(){
	int ret;

	IBProf_Finalize();

#if IBPROF_GET_TIME
	IBPROF_END_TIMER(MPI_COMM_WORLD, "MAIN", 0);
#endif

	ret = PMPI_Finalize();

	return ret;
}

/* Callback function that measures the IB traffic */
int send_cbk(peruse_event_h event_h, MPI_Aint unique_id,
		peruse_comm_spec_t *spec, void *param){
	if(spec->operation == 0){
		/* if it is a send operation, we increment the relevant counter
		 * in the *ibp_ptr structure. frag_type < 2 indicate the send
		 * operation is being measured on the reciever */

		int i;
		struct IBProfiler *ibp_ptr = (struct IBProfiler *) param;
		
		for(i = 0; i < ibp_ptr->num_active_ports; i++){
			if(spec->local_lid == ibp_ptr->lids[i]){
				if(spec->frag_type < 2)
					cntr_add(ibp_ptr->recv_cntrs[i], spec->remote_lid,  spec->count);
				else
					cntr_add(ibp_ptr->send_cntrs[i], spec->remote_lid,  spec->count);
				break;
			}
		}
	}
	return 0;
}

/* Prepares everything necessary to conduct profiling */
int IBProf_Init(char* filename){
	int send_eid;

	/* Initialize OTF variables */
	ibprofiler.otf_manager = OTF_FileManager_open( 10 );
	ibprofiler.otf_stream =OTF_WStream_open(filename, ibprofiler.mpirank+1,
		ibprofiler.otf_manager);

	/* Get environment and node info */
	get_ibp_env();
	get_node_info();

#if IBPROF_WRITE_OTF
	/* Write OTF definitions */
	write_otf_defs(filename);
#endif

	/* Init PERUSE */
	PERUSE_Init();
	PERUSE_Query_event("PERUSE_OPENIB_SEND", &send_eid);
	PERUSE_Query_event("PERUSE_OPENIB_SEND", &send_eid);
	PERUSE_Event_comm_register(send_eid, MPI_COMM_WORLD, send_cbk, 
		&ibprofiler, &ibprofiler.send_ev);

	/* Set up profiling environment */
	if (ibprofiler.trackall == true){
		/* If we're tracing all communication, no need to facilitate
		 * activate-deactivate for specific collective. Activate event 
		 */
		ibprofiler.enable = NULL;
		ibprofiler.disable = NULL;
		PERUSE_Event_activate(ibprofiler.send_ev);
	} else {
		/* If we're not tracing all communication, facilitate activate-
		 * deactivate for specific routines and start with event
		 * deactivated
		 */
		ibprofiler.enable = PERUSE_Event_activate;
		ibprofiler.disable = PERUSE_Event_deactivate;
	}
	
	ibprofiler.inited = 1;
	return 0;
}

/* Pause profiling by deactivating the Peruse event */
int IBProf_Pause(){
	if (ibprofiler.inited == 1){
		ibprofiling = false;
		return PERUSE_Event_deactivate(ibprofiler.send_ev);
	} else
		return 0;
}

/* Fortran interface */
void ibprof_pause_(){
	IBProf_Pause();
}

/* Resume profiling by re-activating the Peruse event */
int IBProf_Resume(){
	if (ibprofiler.inited == 1){
		ibprofiling = true;
		return PERUSE_Event_activate(ibprofiler.send_ev);
	} else
		return 0;
}

/* Fortran interface */
void ibprof_resume_(){
	IBProf_Resume();
}

/* Cleans up the profiling environment */
int IBProf_Finalize(){
	int i;

#if IBPROF_GET_TIME
	IBPROF_BEGIN_TIMER(MPI_COMM_WORLD);
#endif

#if IBPROF_WRITE_OTF
	/* Write profiles to OTF files */
	write_otf_counters();
#endif

#if IBPROF_GET_TIME
	IBPROF_END_TIMER(MPI_COMM_WORLD, "DUMP", 0);
#endif

	/* Deactivate and release variables/handles */
	OTF_WStream_close(ibprofiler.otf_stream);
	OTF_FileManager_close(ibprofiler.otf_manager);

	/* Free counter objects and pointers */
	for(i = 0; i < ibprofiler.num_active_ports; i++){
		cntr_finalize(&ibprofiler.send_cntrs[i]);
		cntr_finalize(&ibprofiler.recv_cntrs[i]);
		
	}

	/* Free counter objects and pointers */
	free(ibprofiler.send_cntrs);
	free(ibprofiler.recv_cntrs);

	free(ibprofiler.pguids);

	PERUSE_Event_deactivate(ibprofiler.send_ev);
	PERUSE_Event_release(&ibprofiler.send_ev);

	ibprofiler.inited = 0;
	return 0;
}

/* Used to mark the end of the code region and writes counters to files.
 * Counters are reset after being written */
int end_region(){
	if (ibprofiler.inited == 1){
		return write_otf_counters();
	} else
		return 0;
}

/* Fortran interface */
void end_region_(){
	end_region();
}

/* Checks environment of operations that should be profiled */
int get_ibp_env(){
	char *coll_list, *token, *max_lid;
	char *delimiters = " -.,:;|";
	
	/* Get env variable: check which collective op should be profiled */
	coll_list = getenv("IBP_OPERATIONS");
	if (coll_list == NULL){
		//printf("KEVOUT: tracing All.\n");
		ibprofiler.trackall = true;
	} else{
		//printf("KEVOUT: tracing %s.\n", coll_list);
		while(coll_list != NULL){
			token = strsep(&coll_list, delimiters);
			if (strcmp_nocase(token, "BCAST") == 0){
				ibprofiler.bcast = true;
			}
			else if (strcmp_nocase(token, "SCATTER") == 0){
				ibprofiler.scatter = true;
			}
			else if (strcmp_nocase(token, "SCATTERV") == 0){
				ibprofiler.scatterv = true;
			}
			else if (strcmp_nocase(token, "GATHER") == 0){
				ibprofiler.gather = true;
			}
			else if (strcmp_nocase(token, "GATHERV") == 0){
				ibprofiler.gatherv = true;
			}
			else if (strcmp_nocase(token, "REDUCE") == 0){
				ibprofiler.reduce = true;
			}
			else if (strcmp_nocase(token, "REDUCE_SCATTER") == 0){
				ibprofiler.reduce_scatter = true;
			}
			else if (strcmp_nocase(token, "ALLTOALL") == 0){
				ibprofiler.alltoall = true;
			}
			else if (strcmp_nocase(token, "ALLTOALLV") == 0){
				ibprofiler.alltoallv = true;
			}
			else if (strcmp_nocase(token, "ALLTOALLW") == 0){
				ibprofiler.alltoallw = true;
			}
			else if (strcmp_nocase(token, "ALLGATHER") == 0){
				ibprofiler.allgather = true;
			}
			else if (strcmp_nocase(token, "ALLGATHERV") == 0){
				ibprofiler.allgatherv = true;
			}
			else if (strcmp_nocase(token, "ALLREDUCE") == 0){
				ibprofiler.allreduce = true;
			}
			else if (strcmp_nocase(token, "SCAN") == 0){
				ibprofiler.scan = true;
			}
			else if (strcmp_nocase(token, "TRACEALL") == 0){
				ibprofiler.trackall = true;
			}
		}
		
	}
	/* Get env variable: check the maximum LID */
	max_lid = getenv("IBP_MAXLID");
	if (max_lid == NULL){
		ibprofiler.max_lid = DEFAULT_MAX_LID;
	} else{
		ibprofiler.max_lid = atoi(max_lid);
	}
	return 0;
}

/* Gets hostname and queries the InfinBand devices for port information
*/ 
int get_node_info() {
	int i, j, lids[20], port_cnt=0;
	char *hostname;
	uint64_t pguids[20];

	struct utsname uname_data;
	struct ibv_device **devs;
	struct ibv_context *devctx;
	struct ibv_device_attr dev_attr;
	struct ibv_port_attr port_attr;
	union ibv_gid gid;

	/* Record system's hostname */
	uname(&uname_data);
	hostname = uname_data.nodename;
	sprintf(ibprofiler.hostname, "%.*s", 
			(int)(sizeof(ibprofiler.hostname)/sizeof(char)) -1, 
			(hostname==NULL)? "default-name":strsep(&hostname,"."));

	/* Interate over all InfiniBand ports and record active ports */
	devs = ibv_get_device_list(NULL);
	for(i = 0; devs[i] != NULL; i++){
		devctx = ibv_open_device(devs[i]);
		ibv_query_device(devctx, &dev_attr);
		for(j = 1; j <= dev_attr.phys_port_cnt; j++){
			ibv_query_port(devctx, j, &port_attr);
			
			if(port_attr.state == IBV_PORT_ACTIVE || 
					port_attr.state == IBV_PORT_ARMED){
				ibv_query_gid(devctx, j, 0, &gid);
				lids[port_cnt] = port_attr.lid;
				pguids[port_cnt] = gid.global.interface_id;

				port_cnt++;
			}
		}
		ibv_close_device(devctx);
	}
	ibv_free_device_list(devs);

	/* Allocate counter buffers for each active port */
	ibprofiler.num_active_ports = port_cnt;
	ALLOC_COUNTER(ibprofiler.send_cntrs);
	ALLOC_COUNTER(ibprofiler.recv_cntrs);
	ibprofiler.lids   = (uint32_t*) malloc(sizeof(uint32_t) * port_cnt);
	ibprofiler.pguids = (uint64_t*) malloc(sizeof(uint64_t) * port_cnt);
	for (i = 0; i < port_cnt; i++){
		ibprofiler.lids[i] = lids[i];
		/* Switch from network byte order to host byte order */
		ibprofiler.pguids[i] = be64toh(pguids[i]);
		ibprofiler.send_cntrs[i] = cntr_init();
		ibprofiler.recv_cntrs[i] = cntr_init();
	}

	return 0;
}

/* Writes OTF definitions such as: hostname, MPI rank, LID, etc */
int write_otf_defs(const char* filename){
	int i;

	/* Write stream mapping and process list from process 0*/
	if (ibprofiler.mpirank == 0){
		OTF_MasterControl* mc;
		OTF_WStream* main_stream;
		uint32_t *procs;
		int i;

		mc = OTF_MasterControl_new( ibprofiler.otf_manager );
		main_stream = OTF_WStream_open( filename, ibprofiler.mpirank,
						ibprofiler.otf_manager );
		procs = (uint32_t*) malloc(ibprofiler.mpisize*(sizeof(uint32_t)));
		
		for (i = 0; i < ibprofiler.mpisize; i++){
			procs[i] = i;
			OTF_MasterControl_append(mc, i+1, i ); 
		}
		OTF_WStream_writeDefTimerResolution(main_stream, 1 );
		OTF_WStream_writeDefFunctionGroup(main_stream, 1, 
			"all functions" );
		OTF_WStream_writeDefFunction(main_stream, 1, "main", 1, 0 );
		OTF_WStream_writeDefProcessGroup(main_stream, 1, 
			"MPI_COMM_WORLD", ibprofiler.mpisize, procs);

		OTF_WStream_close(main_stream );
		OTF_MasterControl_write( mc, filename );
		OTF_MasterControl_close( mc );
		free(procs);
	}
	
	/* Write local MPI rank, LID, PGUID, and hostname from each process */
      	OTF_WStream_writeDefProcess(ibprofiler.otf_stream, ibprofiler.mpirank, 
			ibprofiler.hostname, 0);

	char str[20];
	for (i = 0; i < ibprofiler.num_active_ports; i++){
		sprintf(str, "0x%016"PRIx64"\n", ibprofiler.pguids[i]);
			/* counter_key:	src_lid
	 		 * counter_val: dst_lid */
		OTF_WStream_writeDefCounterKV(ibprofiler.otf_stream, ibprofiler.lids[i], 
			str, OTF_COUNTER_TYPE_ABS + OTF_COUNTER_SCOPE_POINT, 
			ibprofiler.mpirank, "bytes sent", NULL);
	}

	OTF_WStream_writeEnter( ibprofiler.otf_stream, 1, 1, ibprofiler.mpirank, 0 );

	return 0;
} 

/* Write all non-zero port counters to file.
 * Every time a counter set is written, a new code region is created.*/
int write_otf_counters(){
	int i, send_res, recv_res;
	OTF_KeyValueList *KeyValueList;
	KeyValueList = OTF_KeyValueList_new();

	uint32_t dst_lid;
	ibprof_data_t_c data;
	
			/*old:	KV pair:	dst_lid - bytes_sent  
 			  key	val
			  0	bytes sent
			  1	bytes recv */
	for (i = 0; i < ibprofiler.num_active_ports; i++){
		// SEND Counters
		send_res = cntr_get(ibprofiler.send_cntrs[i], &dst_lid, &data);
		while (send_res != 0){
			OTF_KeyValueList_appendUint64( KeyValueList, 0, data);

			recv_res = cntr_find(ibprofiler.recv_cntrs[i], dst_lid, &data);
			if (recv_res != 0){
				OTF_KeyValueList_appendUint64( KeyValueList, 1, data);
			}
			//if (OTF_KeyValueList_getCount(KeyValueList) > 0) {
			//	printf("Writing counter %"PRIu64" to %d.\n", ibprofiler.send_cntrs[POS(i,0)], j);
					/* counter_key:	src_lid
		 			 * counter_val: dst_lid */
			OTF_WStream_writeCounterKV( ibprofiler.otf_stream, otf_code_region, 
				ibprofiler.mpirank, ibprofiler.lids[i],
				dst_lid, KeyValueList );

			//printf("Written traffic from %d -> %d.\n", ibprofiler.lids[i], dst_lid);
			send_res = cntr_get(ibprofiler.send_cntrs[i], &dst_lid, &data);
		}
		// RMA GET (RECV) Counters
		recv_res = cntr_get(ibprofiler.recv_cntrs[i], &dst_lid, &data);
		while (recv_res != 0){
			OTF_KeyValueList_appendUint64( KeyValueList, 1, data);

					/* counter_key:	src_lid
		 			 * counter_val: dst_lid */
			OTF_WStream_writeCounterKV( ibprofiler.otf_stream, otf_code_region, 
				ibprofiler.mpirank, ibprofiler.lids[i],
				dst_lid, KeyValueList );

			recv_res = cntr_get(ibprofiler.send_cntrs[i], &dst_lid, &data);
		}
 	}
	otf_code_region++;
	return 0;
}

