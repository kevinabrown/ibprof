/************************************************************************
Copyright 2014 Kevin A. Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*************************************************************************/

#ifndef IBPROF_I_H
#define IBPROF_I_H

#include <stdbool.h>
#include "mpi.h"
#include "otf.h"
#include "peruse.h"
#include "ibprof_counter.h"

#ifndef DIBPROF_GET_TIME
#define DIBPROF_GET_TIME 0 
#endif

#ifndef DIBPROF_WRITE_OTF
#define DIBPROF_WRITE_OTF 1
#endif

/* ibprofiler.max_lid/MAX_LID is always +1, location 0 of counter array is 
 * used to store source LID 
 * */
#define DEFAULT_MAX_LID 1900
#define POS(x,y) x * ibprofiler.max_lid + y

// Timer Variables - for testing
struct timeval st, et;

// Status variables
bool ibprofiling = true;
// region 0 is reserveed for port counters
int otf_code_region = 1;

// Declare structure that contains all required handles, counters etc.
struct IBProfiler{
	int			inited;
	int			max_lid;		//max lid in the net
	// Peruse event handle and function pointers
	peruse_event_h		send_ev;
	int (*enable)		(peruse_event_h);
	int (*disable)		(peruse_event_h);
	// Counters for data sent to each target lid from each sourse lid
        ibprof_cntr_ptr*	send_cntrs;
        ibprof_cntr_ptr*	recv_cntrs;
	// OTF variables
        OTF_FileManager*        otf_manager;
        OTF_WStream*            otf_stream;
	// Host information
	char			hostname[30];
	int			num_active_ports;	// per host
        uint64_t*               pguids;
        uint32_t*               lids;
        int                     mpirank;
        int                     mpisize;
	// Sequential code region identifier
	int			code_region;
	// Collectives that may be isolated for tracing
	bool			trackall;
	bool			bcast;
	bool			scatter;
	bool			scatterv;
	bool			gather;
	bool			gatherv;
	bool			reduce;
	bool			reduce_scatter;
	bool			alltoall;
	bool			alltoallv;
	bool			alltoallw;
	bool			allgather;
	bool			allgatherv;
	bool			allreduce;
	bool			scan;
};

// Initialize ibprofiler
struct IBProfiler ibprofiler = { 
	.inited = 0,
	.enable = NULL,
	.disable = NULL,
	.trackall=false,
	.bcast=false,
	.scatter=false,
	.scatterv=false,
	.gather=false,
	.gatherv=false,
	.reduce=false,
	.reduce_scatter=false,
	.alltoall=false,
	.alltoallv=false,
	.alltoallw=false,
	.allgather=false,
	.allgatherv=false,
	.allreduce=false,
	.scan=false
	};	

void getArgs(int argc, char** argv, int *TRIALS, int *LENGTH, 
					size_t *factor);

int send_cbk(peruse_event_h event_h, MPI_Aint unique_id,
		peruse_comm_spec_t *spec, void *param);

int IBProf_Init(char* filename);

int IBProf_Finalize();

int get_ibp_env();

int get_node_info();

int write_otf_defs(const char* filename);

int write_otf_counters();

#define IBPROF_CHECK_AND_SET(op)					\
	if (ibprofiling && op){					\
		if (ibprofiler.enable != NULL ){		\
			ibprofiler.enable(ibprofiler.send_ev);	\
		}						\
	}

#define IBPROF_SET_DEFAULT()					\
	if (ibprofiling){						\
		if (ibprofiler.disable != NULL ){			\
			ibprofiler.disable(ibprofiler.send_ev);	\
		}						\
	}

#define IBPROF_BEGIN_TIMER(comm)  			\
	struct timeval st, et;			\
	MPI_Barrier(comm);			\
	gettimeofday(&st, NULL);


#define IBPROF_END_TIMER(comm, name, root) 				\
	MPI_Barrier(comm);						\
	gettimeofday(&et, NULL);					\
									\
	if (ibprofiler.mpirank == root) {				\
		printf("KEVOUT:[%s] microsec:%ld\n", name, 		\
			((et.tv_sec - st.tv_sec)*1000000L + et.tv_usec)	\
			- st.tv_usec);					\
	}

#define ALLOC_COUNTER(x) do{ 						\
	x = (ibprof_cntr_ptr*) malloc(sizeof(ibprof_cntr_ptr) * 	\
					ibprofiler.num_active_ports);	\
}while(0)

#define PRINT_COUNTER_S(s, x) do{					\
	int k;								\
	printf("\n");printf(s);						\
	printf("\nSrc-Dst:Bytes\n");					\
	for(k = 1; k < ibprofiler.max_lid; k++){			\
		if ( x[POS(0,k)] > 0 )					\
			printf("%" PRIu64 ":%d:%" PRIu64 "\n", 		\
				x[POS(0,0)], k, x[POS(0,k)]);		\
	}								\
	for(k = 1; k < ibprofiler.max_lid; k++){			\
		if ( x[POS(1,k)] > 0 )					\
			printf("%" PRIu64 ":%d:%" PRIu64 "\n",		\
				x[POS(1,0)], k, x[POS(1,k)]);		\
	}								\
} while(0)

#define PRINT_COUNTER_R(s, x) do{					\
	int k;								\
	printf("\n");printf(s);						\
	printf("\nSrc-Dst:Bytes\n");					\
	for(k = 0; k < ibprofiler.max_lid; k++){			\
		if ( x[POS(1,k)] > 0 )					\
			printf("%d:%" PRIu64 ":%" PRIu64 "\n",		\
				k, x[POS(0,0)], x[POS(1,k)]);		\
	}								\
} while(0)


#endif
