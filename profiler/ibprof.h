/************************************************************************
Copyright 2014 Kevin A. Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*************************************************************************/

#ifndef IBPROF_H
#define IBPROF_H

#ifdef __cplusplus
extern "C" {
#endif

#define IBPROF_PAUSE() IBProf_Pause();
int IBProf_Pause();

#define IBPROF_RESUME() IBProf_Resume();
int IBProf_Resume();

#define IBPROF_SEGMENT() end_region();
int end_region();

#ifdef __cplusplus
}
#endif

#endif
