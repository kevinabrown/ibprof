/************************************************************************
Copyright 2014 Kevin A. Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*************************************************************************/

#ifndef IBPROF_PMPI_H
#define IBPROF_PMPI_H

int MPI_Alltoall(void *sendbuff, int sendcount, MPI_Datatype sendtype, 
		void *recvbuff, int recvcount, MPI_Datatype recvtype,
	MPI_Comm comm){
	int ret;

	IBPROF_CHECK_AND_SET(ibprofiler.alltoall);

#if IBPROF_GET_TIME
//	IBPROF_BEGIN_TIMER(comm);
#endif

	ret = PMPI_Alltoall(sendbuff, sendcount, sendtype, recvbuff, recvcount,
				recvtype, comm);

#if IBPROF_GET_TIME
//	IBPROF_END_TIMER(comm, "ALLTOALL", 0);
#endif

	IBPROF_SET_DEFAULT();

	return ret;
}

int MPI_Alltoallv(void *sendbuff, int *sendcounts, int *sdispls,
		MPI_Datatype sendtype, void *recvbuff, int *recvcounts,
		int *rdispls, MPI_Datatype recvtype, MPI_Comm comm){
	int ret;

	IBPROF_CHECK_AND_SET(ibprofiler.alltoallv);

#if IBPROF_GET_TIME
//	IBPROF_BEGIN_TIMER(comm);
#endif

	ret = PMPI_Alltoallv(sendbuff, sendcounts, sdispls, sendtype, recvbuff,
			recvcounts, rdispls, recvtype, comm);

#if IBPROF_GET_TIME
//	IBPROF_END_TIMER(comm, "ALLTOALLV", 0);
#endif

	IBPROF_SET_DEFAULT();

	return ret;
}

int MPI_Alltoallw(void *sendbuff, int sendcounts[], int sdispls[], MPI_Datatype sendtype[],
		void *recvbuff, int recvcounts[], int rdispls[], MPI_Datatype recvtype[],  MPI_Comm comm){
	int ret;

	IBPROF_CHECK_AND_SET(ibprofiler.alltoallw);

#if IBPROF_GET_TIME
//	IBPROF_BEGIN_TIMER(comm);
#endif

	ret = PMPI_Alltoallw(sendbuff, sendcounts, sdispls, sendtype, recvbuff,
			recvcounts, rdispls, recvtype, comm);

#if IBPROF_GET_TIME
//	IBPROF_END_TIMER(comm, "ALLTOALLW", 0);
#endif

	IBPROF_SET_DEFAULT();

	return ret;
}

int MPI_Bcast(void *buffer, int count, MPI_Datatype datatype, int root,
	MPI_Comm comm){
	int ret;

	IBPROF_CHECK_AND_SET(ibprofiler.bcast);

#if IBPROF_GET_TIME
//	IBPROF_BEGIN_TIMER(comm);
#endif

	ret = PMPI_Bcast(buffer, count, datatype, root, comm);

#if IBPROF_GET_TIME
//	IBPROF_END_TIMER(comm, "BCAST", root);
#endif

	IBPROF_SET_DEFAULT();

	return ret;
}

int MPI_Reduce(void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype,
	MPI_Op op, int root, MPI_Comm comm){
	int ret;

	IBPROF_CHECK_AND_SET(ibprofiler.reduce);

#if IBPROF_GET_TIME
//	IBPROF_BEGIN_TIMER(comm);
#endif

	ret = PMPI_Reduce(sendbuf, recvbuf, count, datatype, op, root, comm);

#if IBPROF_GET_TIME
//	IBPROF_END_TIMER(comm, "REDUCE", root);
#endif

	IBPROF_SET_DEFAULT();

	return ret;
}

int MPI_Allreduce(void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype,
	MPI_Op op, MPI_Comm comm){
	int ret;

	IBPROF_CHECK_AND_SET(ibprofiler.allreduce);

#if IBPROF_GET_TIME
//	IBPROF_BEGIN_TIMER(comm);
#endif

	ret = PMPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm);

#if IBPROF_GET_TIME
//	IBPROF_END_TIMER(comm, "ALLREDUCE", 0);
#endif

	IBPROF_SET_DEFAULT();

	return ret;
}

int MPI_Reduce_scatter(void *sendbuf, void *recvbuf, int recvcounts[], MPI_Datatype datatype,
	MPI_Op op, MPI_Comm comm){
	int ret;

	IBPROF_CHECK_AND_SET(ibprofiler.reduce_scatter);

#if IBPROF_GET_TIME
//	IBPROF_BEGIN_TIMER(comm);
#endif

	ret = PMPI_Reduce_scatter(sendbuf, recvbuf, recvcounts, datatype, op, comm);

#if IBPROF_GET_TIME
//	IBPROF_END_TIMER(comm, "REDUCE_SCATTER", 0);
#endif

	IBPROF_SET_DEFAULT();

	return ret;
}

int MPI_Scatter(void *sendbuf, int sendcount, MPI_Datatype sendtype, 
		void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm){
	int ret;

	IBPROF_CHECK_AND_SET(ibprofiler.scatter);

#if IBPROF_GET_TIME
//	IBPROF_BEGIN_TIMER(comm);
#endif

	ret = PMPI_Scatter(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, root, comm);

#if IBPROF_GET_TIME
//	IBPROF_END_TIMER(comm, "SCATTER", root);
#endif

	IBPROF_SET_DEFAULT();

	return ret;
}

int MPI_Scatterv(void *sendbuf, int sendcounts[], int displs[], MPI_Datatype sendtype, 
		void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm){
	int ret;

	IBPROF_CHECK_AND_SET(ibprofiler.scatterv);

#if IBPROF_GET_TIME
//	IBPROF_BEGIN_TIMER(comm);
#endif

	ret = PMPI_Scatterv(sendbuf, sendcounts, displs, sendtype, recvbuf, recvcount, recvtype, root, comm);

#if IBPROF_GET_TIME
	//IBPROF_END_TIMER(comm, "SCATTERV", root);
#endif

	IBPROF_SET_DEFAULT();

	return ret;
}

int MPI_Gather(void *sendbuf, int sendcount, MPI_Datatype sendtype, 
		void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm){
	int ret;

	IBPROF_CHECK_AND_SET(ibprofiler.gather);

#if IBPROF_GET_TIME
//	IBPROF_BEGIN_TIMER(comm);
#endif

	ret = PMPI_Gather(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, root, comm);

#if IBPROF_GET_TIME
	//IBPROF_END_TIMER(comm, "GATHER", root);
#endif

	IBPROF_SET_DEFAULT();

	return ret;
}

int MPI_Gatherv(void *sendbuf, int sendcount, MPI_Datatype sendtype, 
		void *recvbuf, int recvcounts[], int displs[], MPI_Datatype recvtype, 
		int root, MPI_Comm comm){
	int ret;

	IBPROF_CHECK_AND_SET(ibprofiler.gatherv);

#if IBPROF_GET_TIME
//	IBPROF_BEGIN_TIMER(comm);
#endif

	ret = PMPI_Gatherv(sendbuf, sendcount, sendtype, recvbuf, recvcounts, displs, recvtype, root, comm);

#if IBPROF_GET_TIME
	//IBPROF_END_TIMER(comm, "GATHERV", root);
#endif

	IBPROF_SET_DEFAULT();

	return ret;
}

int MPI_Allgather(void *sendbuf, int sendcount, MPI_Datatype sendtype, 
		void *recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm){
	int ret;

	IBPROF_CHECK_AND_SET(ibprofiler.allgather);

#if IBPROF_GET_TIME
//	IBPROF_BEGIN_TIMER(comm);
#endif

	ret = PMPI_Allgather(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm);

#if IBPROF_GET_TIME
	//IBPROF_END_TIMER(comm, "ALLGATHER", 0);
#endif

	IBPROF_SET_DEFAULT();

	return ret;
}

int MPI_Allgatherv(void *sendbuf, int sendcount, MPI_Datatype sendtype, 
		void *recvbuf, int recvcounts[], int displs[], MPI_Datatype recvtype, 
		MPI_Comm comm){
	int ret;

	IBPROF_CHECK_AND_SET(ibprofiler.allgatherv);

#if IBPROF_GET_TIME
//	IBPROF_BEGIN_TIMER(comm);
#endif

	ret = PMPI_Allgatherv(sendbuf, sendcount, sendtype, recvbuf, recvcounts, displs, recvtype, comm);

#if IBPROF_GET_TIME
	//IBPROF_END_TIMER(comm, "ALLGATHERV", 0);
#endif

	IBPROF_SET_DEFAULT();

	return ret;
}

int MPI_Scan(void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype,
	MPI_Op op, MPI_Comm comm){
	int ret;

	IBPROF_CHECK_AND_SET(ibprofiler.scan);

#if IBPROF_GET_TIME
//	IBPROF_BEGIN_TIMER(comm);
#endif

	ret = PMPI_Scan(sendbuf, recvbuf, count, datatype, op, comm);

#if IBPROF_GET_TIME
	//IBPROF_END_TIMER(comm, "SCAN", 0);
#endif

	IBPROF_SET_DEFAULT();

	return ret;
}

#endif

