/********************************************************
 * 	This code uses the uint64_t. 
 * 	Ensure that your C++ compiler supports it.
 *******************************************************/
#ifndef IBPROF_CNTR_H
#define IBPROF_CNTR_H

#ifdef __cplusplus
#define EXTERNC extern "C"
#include <cstdint>

#else
#define EXTERNC

#endif

#include <inttypes.h>

typedef uint64_t ibprof_data_t_cpp;
typedef uint64_t ibprof_data_t_c;
typedef void* ibprof_cntr_ptr;

EXTERNC ibprof_cntr_ptr cntr_init(void);
EXTERNC void cntr_clear(ibprof_cntr_ptr ptr);
EXTERNC void cntr_add(ibprof_cntr_ptr ptr, uint32_t dst_lid, ibprof_data_t_cpp data);
EXTERNC int cntr_get(ibprof_cntr_ptr ptr, uint32_t *dst_lid, ibprof_data_t_cpp *data);
EXTERNC int cntr_find(ibprof_cntr_ptr ptr, const uint32_t dst_lid, ibprof_data_t_cpp *data);
EXTERNC void cntr_finalize(ibprof_cntr_ptr *cntr_ptr);
EXTERNC void cntr_print(ibprof_cntr_ptr ptr);

struct CNTR_Ops{
	ibprof_cntr_ptr	(*init)(void);
	void		(*clear)(ibprof_cntr_ptr ptr);
	void		(*add)(ibprof_cntr_ptr cntr_ptr, int dst_lid, ibprof_data_t_cpp data);
	void		(*finalize)(ibprof_cntr_ptr cntr_ptr);
};
/*
struct CNTR_ops cntr_ops = {
	.init 		= &cntr_init,
	.clear		= &cntr_clear,
	.add		= &cntr_add,
	.finalize	= &cntr_finalize
}; */

#ifdef __cplusplus
#else
#endif

#undef EXTERNC

#endif
