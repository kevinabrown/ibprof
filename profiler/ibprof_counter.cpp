/********************************************************
 * 	This code uses the uint64_t. 
 * 	Ensure that your C++ compiler supports it.
 *******************************************************/

#include <unordered_map>
#include <iostream>
#include "ibprof_counter.h"

typedef std::unordered_map<int, ibprof_data_t_cpp> ibprof_cntr_t;
typedef ibprof_cntr_t::const_iterator ibprof_cntr_item;

ibprof_cntr_ptr cntr_init(void){
	ibprof_cntr_t *cntr = new ibprof_cntr_t;

	return static_cast<void*>(cntr);
}

void cntr_finalize(ibprof_cntr_ptr *cntr_ptr){
	ibprof_cntr_t *cntr = static_cast<ibprof_cntr_t*>(*cntr_ptr);

	delete cntr;
	*cntr_ptr = NULL;
//	std::cout << "FINALIZED." << std::endl;
}

void cntr_clear(ibprof_cntr_ptr ptr){
	ibprof_cntr_t *cntr_ptr = static_cast<ibprof_cntr_t*>(ptr);

	cntr_ptr->clear();
}

void cntr_add(ibprof_cntr_ptr ptr, uint32_t dst_lid, ibprof_data_t_cpp data){

	ibprof_cntr_t *cntr_ptr = static_cast<ibprof_cntr_t*>(ptr);

	ibprof_cntr_item element = cntr_ptr->find(dst_lid);
	if (element == cntr_ptr->end()){
		std::pair <int, ibprof_data_t_cpp> entry (dst_lid, data);
		cntr_ptr->insert(entry);
	}
	else{
		(*cntr_ptr)[dst_lid] = (*cntr_ptr)[dst_lid] + data;
	}
	
//	element = cntr_ptr->find(dst_lid);
//	std::cout << "ADDED: " << element->first << " - " << element->second << std::endl;
}

/* Populates parameters with the element found and returns 1,
 * else returns 0
 */
int cntr_get(ibprof_cntr_ptr ptr, uint32_t *dst_lid, ibprof_data_t_cpp *data){
	ibprof_cntr_t *cntr_ptr = static_cast<ibprof_cntr_t*>(ptr);

	if (cntr_ptr->begin() != cntr_ptr->end()){
		*dst_lid = cntr_ptr->begin()->first;
		*data = cntr_ptr->begin()->second;
//		std::cout << "GOT: " << cntr_ptr->begin()->first << " - " << cntr_ptr->begin()->second << std::endl;
		cntr_ptr->erase(cntr_ptr->begin());
		return 1;
	} else{
		*dst_lid = 0;
		*data = 0;
//		std::cout << "GOT: None" << std::endl;
		return 0;
	}
}

/* If element is found, populates data and return 1,
 * else returns 0
 */
int cntr_find(ibprof_cntr_ptr ptr, const uint32_t dst_lid, ibprof_data_t_cpp *data){
	ibprof_cntr_t *cntr_ptr = static_cast<ibprof_cntr_t*>(ptr);

	ibprof_cntr_item element = cntr_ptr->find(dst_lid);
	if (element == cntr_ptr->end()){
		*data = 0;
//		std::cout << "REMOVED: " << "None" << std::endl;
		return 0;
	}
	else{
		*data = (*cntr_ptr)[dst_lid];
//		std::cout << "REMOVED: " << cntr_ptr->begin()->first << " - " << cntr_ptr->begin()->second << std::endl;
		cntr_ptr->erase(element);
		return 1;
	}
}

void cntr_print(ibprof_cntr_ptr ptr){
	ibprof_cntr_t *cntr_ptr = static_cast<ibprof_cntr_t*>(ptr);

	ibprof_cntr_t::iterator it;
	for ( it = cntr_ptr->begin(); it != cntr_ptr->end(); it++ )
	    std::cout << "Element: " << it->first << " - " << it->second << std::endl;
}
